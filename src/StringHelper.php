<?php

/*
 * Copyright (C) 2021 qerana@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\Helpers;

/**
 * Utilidades para string
 *
 * @author tau@qerana.com
 */
class StringHelper
{

    /**
     * Limpia string
     * 
     * Reemplaza tildes y caracteres raros por nada
     * @param StringHelper $string
     * @return StringHelper
     */
    public static function cleanString(StringHelper $string): StringHelper
    {
        $text = strtolower($string);
        $patron = array(
            // Espacios, puntos y comas por guion
            '/[\., ]+/' => '-',
            // Vocales
            'á' => 'a',
            'é' => 'e',
            'í' => 'i',
            'ó' => 'o',
            'ú' => 'u',
            'ñ' => 'n',
            'Á' => 'a',
            'É' => 'e',
            'Í' => 'i',
            'Ó' => 'o',
            'Ú' => 'u',
            'Ñ' => 'n'
                // Agregar aqui mas caracteres si es necesario
        );

        return str_replace(array_keys($patron), array_values($patron), $text);
    }

    /**
     * Reemplaza en un string un patron
     * @param StringHelper $string
     * @param array $pattern
     * @return StringHelper
     */
    public static function replaceStringPattern(StringHelper $string, array $pattern): StringHelper
    {
        return str_replace(array_keys($pattern), array_values($pattern), $string);
    }

    /**
     * Parsea un array en json
     * @param mixed $array
     */
    public static function parseToJson($array)
    {

        $json_response = [];
        $is_a_array = (is_array($array)) ? true : false;
        $n_elements = ($is_a_array) ? count($array) : 1;

        // si es un array
        if ($is_a_array) {
            if ($n_elements > 0) {

                $json_response['num_reg'] = $n_elements;
                $json_response['exists'] = true;
                $json_response['record'] = $array;
            } else {
                $json_response['exists'] = false;
            }
        } 
        
        // si es un objeto
        else if (is_object($array)) {
            $json_response['num_reg'] = 1;
            $json_response['exists'] = true;
            foreach ($array AS $k => $v):
                $json_response[$k] = $v;
            endforeach;
        } 
        
        // si no es vacio
        else {
            $json_response['exists'] = false;
        }


        echo json_encode($json_response);
    }
    
    /**
     * Comprueba que un string de tipo email sea valido
     * 
     * Opcionalmente comprueba si el dominio tiene registros MX
     * @param StringHelper $email , el email a comprobar
     * @param bool $mx , si es true, comprueba las dns del dominio
     * @return StringHelper , el email
     * @throws \InvalidArgumentException
     */
    public static function checkEmail(StringHelper $email,bool $mx = false)
    {

        if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            throw new \InvalidArgumentException($email . ' is not valid email!!');
        } 
        
        // si mx es true, comprobamos el estado del dominio
        else if($mx) {
            // check email domain
            $domain = explode('@', $email);

            if (!checkdnsrr($domain[1])) {
                throw new \InvalidArgumentException('EmailValidator: ' . $email
                . ' domain mx records fail');
            }
        }
        return trim($email);
    }

}
