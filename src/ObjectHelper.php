<?php

/*
 * Copyright (C) 2021 qerana@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\Helpers;

/**
 * Utilidades para trabajar con objetos
 *
 * @author tau@qerana.com
 */
class ObjectHelper
{

    /**
     * Convierte un objeto en array
     * @param object $Object , el objeto a convertir
     * @param bool $sub , si los atributos tienen un prefijo, esto lo sustrae
     * @return array
     */
    public static function convertToArray(object $Object, bool $sub = true): array
    {

        // to array
        $array = (array) $Object;
        $array_return = [];

        foreach ($array AS $k => $v):

            // replace all private/protected attribute mark 
            $key = trim(str_replace(['*'], '', $k));
            // substract the _ from the begin of properti
            if ($sub) {
                $key = (!is_object($v)) ? substr($key, 1) : $k;
            }

            // is $v is object
            if (is_object($v)) {
                $v = self::convertToArray($v, $sub);
            }

            $array_return[$key] = $v;
        endforeach;


        return $array_return;
    }

    /**
     * Suma una propiedad de un objeto en un array de objetos
     * @param array $array array de objetos
     * @param string $property , la propiedad que queremos utilizar para sumar
     * @return mixed resutlado de la suma
     */
    public static function sumProperty(array $array, string $property)
    {

        $sum = 0;
        foreach ($array as $object) {
            $sum += isset($object->{$property}) ? $object->{$property} : 0;
        }

        return $sum;
    }

    /**
     * Busca un valor de una propiedad en un array de objetos
     * @param array $array
     * @param string $property
     * @param type $value
     * @return array
     */
    public static function searchValueObject(array $array, string $property, $value): array
    {
        $result = [];
        foreach ($array as $Object) {
            if ($Object->{$property} == $value) {
                $result[] = $Object;
            }
        }
        return $result;
    }

}
