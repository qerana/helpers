<?php

/*
 * Copyright (C) 2021 qerana@qerana.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerana\Helpers;

/**
 * Utilidades para el manejo de fechas
 *
 * @author tau@qerana.com
 */
class DateHelper
{

    /**
     *  Convierte una fecha en un formato dado
     * @param string $date
     * @param string $format
     * @return type
     * @throws \InvalidArgumentException
     */
    public static function formatDate(string $date, string $format = 'd-m-Y')
    {

        $Date = date_create($date);
        if (!is_object($Date)) {
            throw new \InvalidArgumentException('Invalid date ');
        }

        return date_format($Date, $format);
    }

    /**
     *  La diferencia entre 2 fechas
     *   @param type $date1
     *   @param type $date2
     *   @param string $format , output format
      @url: https://www.php.net/manual/en/dateinterval.format.php
     */
    public static function diffDate($date1, $date2, $format = '%R%a')
    {

        $Date1 = new \DateTime($date1);
        $Date2 = new \DateTime($date2);
        $Diff = $Date1->diff($Date2);

        return $Diff->format($format);
    }

    /**
     *  Devuelve un array asociativo de meses en castellano
     * @return array
     */
    public static function getMonthRange(): array
    {

        return [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        ];
    }

}
